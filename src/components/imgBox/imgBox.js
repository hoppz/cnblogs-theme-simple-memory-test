/**
 * UPDATES AND DOCS AT: https://github.com/BNDong
 * https://www.cnblogs.com/bndong/
 * @author: BNDong, dbnuo@foxmail.com
 * ----------------------------------------------
 * @describe: 图片灯箱处理
 */
import "../../vendor/fancybox/jquery.fancybox";
import "../../vendor/fancybox/jquery.fancybox.min.css";

export default function main(_) {
    const cpb    = $('#cnblogs_post_body')
        ,imgList = $('#cnblogs_post_body img');

    if (cpb.length > 0 && imgList.length > 0) {
        $.each(imgList, (i) => {
            let tem = $(imgList[i]);
            if (!tem.hasClass('code_img_closed') && !tem.hasClass('code_img_opened')) {
                tem.after('<a data-fancybox="gallery" href="'+tem.attr('src')+'"><img src="'+tem.attr('src')+'"/></a>');
                tem.remove();
            }
        });
    }
}